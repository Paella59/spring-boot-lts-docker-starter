# spring-boot-lts-docker-starter

Spring Boot Docker starter is Spring project (2.72 version) with Docker stack, using a CI (Gitlab-CI here).

## Prerequisites for local installation
Links for Windows
- [Maven](https://dlcdn.apache.org/maven/maven-3/3.8.6/binaries/apache-maven-3.8.6-bin.zip)
- [JDK 17](https://adoptium.net/download)
- [Docker](https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe?utm_source=docker&utm_medium=webreferral&utm_campaign=dd-smartbutton&utm_location=header)


## Tree view
```bash
app/
starter/
Dockerfile
...
.gitlab-ci.yml
docker-compose.yml
README.md
```

## Installation
Run the following command in folder app/starter `mvn clean install`
Next, run the following command in the root project folder `docker-compose build && docker-compose up -d`


## Gitlab CI
He test, call SonarCloud for quality gate and build the docker for deploy into Heroku.

### Heroku
Create your accoung and app in [Heroku](https://dashboard.heroku.com/login)
You need to add CI variable in masked and not protected:
- HEROKU_API_KEY : token for login ([get it](https://help.heroku.com/PBGP6IDE/how-should-i-generate-an-api-key-that-allows-me-to-use-the-heroku-platform-api))
- HEROKU_APP_STAGING : project name of dev environment
- HEROKU_APP_PRODUCTION : project name of production environment

![CI variable Heroku](https://ibb.co/vvNPRVd)

### SonarCloud
Create your project in [SonarCloud web site](https://sonarcloud.io/)
Add in CI variable on your repositories, following the [official documentation](https://docs.sonarcloud.io/getting-started/gitlab/) in masked and not protected :
- organization : name of your organization
- projectKey : name of your project
- SONAR_HOST_URL : URL of your sonar client
- SONAR_TOKEN : sonar token for login


![CI variable SonarCloud](https://ibb.co/tD5qMbV "a title")

### Stage
Stage present in CI :
- tests : launch `mvn test` command
- quality : call SonarCloud to have quality gate
- build : creation of war archive
- build-docker : launch `docker-compose build` from root folder
- deploy-dev : deploy result of build to heroku dev
- deploy-prod :deploy result of build to heroku prod
<br/>

CI for each branch :

- dev : tests , quality , build, build-docker, deploy-dev
- main : tests , quality , build, build-docker, deploy-prod
- merge request : tests , quality
- other : tests , quality

# Rules to develop

### Git Flow
This project use GitFlow, for most information, you can see the [official documentation](https://www.atlassian.com/fr/git/tutorials/comparing-workflows/gitflow-workflow).
You must :
- create ticket in kanban board
- create branch for your need from dev 
  - if issue :F_number : title
  - if bug/ hotfix : H_number : title
- create MR into dev, with squash and delete source branch
- finally, you can if you want, merge dev in main branch

### Definition of ready
:black_square_button: create ticket with title and description <br />
:black_square_button: assign ticket to people <br />
:black_square_button: create branch of your ticket from dev

### Definition of done
:black_square_button: Create Merge Request into dev <br />
:black_square_button: Quality Gate have 40% minimum code coverrage <br />
:black_square_button: No smell code
