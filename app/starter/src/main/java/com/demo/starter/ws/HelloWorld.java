package com.demo.starter.ws;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/helloWorld")
public class HelloWorld {

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public String helloWorld(){
        return "Hello World";
    }
}
